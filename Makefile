# this assumes you have setup public key authorization
# to remote host

# change this to the server address
SERVER = 192.168.2.35
USER = root

# don't include node modules or the intellij directory
#FILES = $(shell find ./ -not -path "*//node_modules/*" -not -path "*//.idea/*" -not -path ".//node_modules" -not -name "archive.tar")
FILES = src package.json package-lock.json Makefile

# get the current relative directory and replicate it on the remote host
DEST_DIR = "~/projects/$(notdir $(shell pwd))"

deploy:
	#tar -cfv project_bundle.tar ${FILES}
	@ssh ${USER}@${SERVER} "mkdir -p ${DEST_DIR}"
	#scp -r project_bundle.tar ${USER}@${SERVER}:${DEST_DIR}
	@scp -r ${FILES} ${USER}@${SERVER}:${DEST_DIR}
	#ssh ${USER}@${SERVER}:

testmake:
	@echo ${FILES}
	@echo ${DEST_DIR}
