/**
 * updates the input elements associated with the specified dimensions
 * @param data is an object that contains keys that pertain to data elements
 *        {x: 3, time:32.00} would target elements with ids of x-data and time-data in the html
 */

var defaultBackground;

function updateMeasurements(data) {
    Object
        .keys(data)
        .forEach(function(d) {
            var element = document.getElementById(d + '-data');
            if (element) {
                element.value = data[d].toString();
            }
    });
}

function updateBackground(data) {
    document.body.style.backgroundColor = data.buttonDown? data.color : defaultBackground;
}

function randomWhole(lo, hi) {
    var range = hi - lo + 1;
    return Math.floor(Math.random() * range) + lo;
}

function displayMotionSensorEvt(data) {
    var images = ['static/images/baby-duck.jpg',
        'static/images/seal-xlarge_trans_NvBQzQNjv4BqRtvZRSV037_kYj9aGppl8KoSQRPSZoKNSSXquGEEDe0.jpg'];

    var i = randomWhole(0, images.length - 1);
    if (data.motionSensorVal) {
        document.getElementById('motion-event-display').style.display = 'block';
        document.getElementById('motion-image').src = images[i];
    } else {
        document.getElementById('motion-event-display').style.display = 'none';
    }
}

document.addEventListener('DOMContentLoaded', function(evt) {
    defaultBackground = document.body.style.backgroundColor;
    var ws = new WebSocket('ws://' + location.host);

    ws.addEventListener('open', function() {
        ws.addEventListener('message', function(evt) {
            var data = JSON.parse(evt.data);
            if (data.hasOwnProperty('buttonDown'))
                updateBackground(data);
            else if (data.hasOwnProperty('motionSensorVal'))
                displayMotionSensorEvt(data);
            else
                updateMeasurements(data);
        })
    });
});
