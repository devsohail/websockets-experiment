const util = require('util');
const EventEmitter = require('events');

function randomWhole(lo, hi) {
    var range = hi - lo + 1;
    return Math.floor(Math.random() * range) + lo;
}

var oneToTen = randomWhole.bind(null, 1, 10);

function readData() {
    return {x: oneToTen(), y: oneToTen(), z: oneToTen()}
}

function processData(data) {
    return data;
}

function getData() {
    var data = readData();
    return processData(data);
}

module.exports = (function Sensor() {
    function _Sensor() {
        EventEmitter.call(this);
        var self = this;
        this.dataInterval = setInterval(function() {
            console.log('triple');
            self.emit('triple-axis-event', getData());
        }, 1000);

        this.buttonInterval = setInterval(function() {
            self.emit('button-down');
            console.log('button-down');
            setTimeout(function() {
                console.log('button-up');
                self.emit('button-up');
            }, 1000);
        }, 3000);
    }

    util.inherits(_Sensor, EventEmitter); // inherit the methods 

    return _Sensor;

})();
