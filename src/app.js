const express = require('express')
    , path = require('path')
    , WebSocket = require('ws')
    , bodyParser = require('body-parser');

app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use('/static', express.static(path.join(__dirname, 'public')));

app.use('/', function(req, res, next) {
    res.render('index');
});

module.exports = app;