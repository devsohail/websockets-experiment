const url = require('url');
const WebSocket = require('ws');
var Sensor; 

if (parseInt(process.env.IS_DEV, 10) == 1)
    Sensor = require('./mock-sensor-data')
else
    Sensor = require('./sensor-data')



function clientClosedConnection(ws, req, sensor) {
   return function () {
       sensor.stopCollection();
       delete sensor;
       console.log('client closes connection');
       // give it some time to propagate;
       setTimeout(function() {
           ws.close();
       }, 1000);
   }
}

function handleWSClient(ws, req) {
    console.log('connected web sockets');
    const location = url.parse(req.url, true);
    var sensor = new Sensor();

    ws.on('close', clientClosedConnection(ws, req, sensor));

    sensor.on('redButton-toggle', function(value) {
        try {
            ws.send(JSON.stringify({
                buttonDown: value,
                color: 'red'
            }));
        } catch (err) {
            console.log('bown unknown error');
            sensor.stopCollection();
            ws.close();
        }
    });

    sensor.on('blueButton-toggle', function(value) {
        try {
            ws.send(JSON.stringify({
                buttonDown: value,
                color: 'blue'
            }));
        } catch (err) {
            sensor.stopCollection();
            ws.close();
        }
    });

    sensor.on('yellowButton-toggle', function(value) {
        try {
            ws.send(JSON.stringify({
                buttonDown: value,
                color: 'yellow'
            }));
        } catch (err) {
            sensor.stopCollection();
            ws.close();
        }
    });

    sensor.on('motionSensor-toggle', function(value) {
        try {
            ws.send(JSON.stringify({
                motionSensorVal: value
            }));
        } catch(err) {
            sensor.stopCollection();
            ws.close();
        }
    });

    sensor.on('triple-axis-event', function(data) {
        try {
            ws.send(JSON.stringify(data));
        } catch (err) {
            console.log('triple unknown error');
            sensor.stopCollection();
            ws.close();
        }
    })
}

module.exports = function registerWebsocket(server) {
    const wss = new WebSocket.Server({ server: server});
    wss.on('connection', handleWSClient);
};
