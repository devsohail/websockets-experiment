const util = require('util');
const EventEmitter = require('events');

const mraa = require('mraa');
const RED_BUTTON_PIN = 7;
const BLUE_BUTTON_PIN = 8;
const YELLOW_BUTTON_PIN = 9;
const MOTION_SENSOR_PIN = 10;

const X_SENSOR_PIN = 3
    , Y_SENSOR_PIN = 4
    , Z_SENSOR_PIN = 5;

var red_button = new mraa.Gpio(RED_BUTTON_PIN);
red_button.dir(mraa.DIR_IN);

var blueButton = new mraa.Gpio(BLUE_BUTTON_PIN);
blueButton.dir(mraa.DIR_IN);

var yellowButton = new mraa.Gpio(YELLOW_BUTTON_PIN);
yellowButton.dir(mraa.DIR_IN);

var motionSensor = new mraa.Gpio(MOTION_SENSOR_PIN);
motionSensor.dir(mraa.DIR_IN);

var   xSensor = new mraa.Aio(X_SENSOR_PIN)
    , ySensor = new mraa.Aio(Y_SENSOR_PIN)
    , zSensor = new mraa.Aio(Z_SENSOR_PIN);

const EPSILON = 0.01;

function getMaxIdOfUnnamed(intervalObj) {
    return Math.max.apply(null, 
        Object.keys(intervalObj).filter(function(k) {
            return k.startsWith('unnamed');
        })
        .map(function(d) {
            return parseInt(d.substring(d.lastIndexOf('_') + 1), 10)
        })
    )
}

function makeToggleReader(name, sensor, period) {
    var self = this;
    this.intervals[name] = setInterval(function() {
        setTimeout(function() {
            var curValue = sensor.read();
            if (curValue !== self.sensors[name].currentValue) {
                self.sensors[name].currentValue = curValue;
                self.emit(name + '-toggle', curValue);
            }
        }, 1)

    }, period)
}

function makeReader(name, sensor, period, epsilon) {


}

function DataReadingEmitter (sensor, params) {
    this.intervals= this.intervals || {};
    this.emitters = this.emitters  || {};
    this.sensors = this.sensors || {};
    var period = params.period || 100;
    var name = params.name || 'unnamed_' + getMaxIdOfUnnamed(this.intervals);
    var isToggle = params.isToggle || false;
    var initialValue = params.initialValue || 0;
    var epsilon = params.epsilon || 0.0001;
    this.sensors[name] = {
        currentValue: initialValue,
        sensor: sensor
    }
    if (isToggle) {
        makeToggleReader.call(this, name, sensor, period);
    } else {
        makeReader.call(this, name, sensor, period, epsilon);
    }
}

module.exports = (function Sensor() {
    function _Sensor() {
        var self = this;
        this.intervals = {};
        this.sensors = {};

        EventEmitter.call(this);
        self.tripleAxisData = {
            x: 0,
            y: 0,
            z: 0
        };

        DataReadingEmitter.call(this, red_button, {
            name: 'redButton',
            period: 5,
            isToggle: true
        })

        DataReadingEmitter.call(this, blueButton, {
            name: 'blueButton',
            period: 5,
            isToggle: true
        })

        DataReadingEmitter.call(this, yellowButton, {
            name: 'yellowButton',
            period: 5,
            isToggle: true
        })

        DataReadingEmitter.call(this, motionSensor, {
            name: 'motionSensor',
            period: 500,
            isToggle: true
        });

        this.tripleAxisInterval = setInterval(function() {
            var currentData = {
                x :  xSensor.readFloat(),
                y :  ySensor.readFloat(),
                z :  zSensor.readFloat()
            };
            for (var keys = Object.keys(currentData), i = 0; i < keys.length; i++) {
                k = keys[i];
                if (Math.abs(currentData[k] -  self.tripleAxisData[k]) >= EPSILON) {
                    delete self.tripleAxisData;
                    self.tripleAxisData = currentData;
                    self.emit('triple-axis-event', currentData);
                }
            }
        }, 5);

        this.stopCollection = function() {
            Object.keys(this.intervals).forEach( function(sensorName) {
                clearInterval(self.intervals[sensorName]);
            });

            clearInterval(self.tripleAxisInterval);
            console.log("stopped collection");
        }

    }

    util.inherits(_Sensor, EventEmitter);
    return _Sensor;
})();
